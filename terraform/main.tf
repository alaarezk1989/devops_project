provider "kubernetes" {
  config_path = "~/.kube/config" # Path to your kubeconfig file
}

resource "kubernetes_namespace" "build" {
  metadata {
    name = "build"
  }
  lifecycle {
    ignore_changes = all

  }
}

# resource "kubernetes_service_account" "nexus" {
#   metadata {
#     name      = "nexus"
#     namespace = kubernetes_namespace.build.metadata[0].name
#   }
# }

# resource "kubernetes_role" "nexus" {
#   metadata {
#     name      = "nexus"
#     namespace = kubernetes_namespace.build.metadata[0].name
#   }

#   rule {
#     api_groups = [""]
#     resources  = ["pods", "services", "configmaps", "secrets"]
#     verbs      = ["*"]
#   }
# }

# resource "kubernetes_role_binding" "nexus" {
#   metadata {
#     name      = "nexus"
#     namespace = kubernetes_namespace.build.metadata[0].name
#   }

#   role_ref {
#     api_group = "rbac.authorization.k8s.io"
#     kind      = "Role"
#     name      = kubernetes_role.nexus.metadata[0].name
#   }

#   subject {
#     kind      = "ServiceAccount"
#     name      = kubernetes_service_account.nexus.metadata[0].name
#     namespace = kubernetes_namespace.build.metadata[0].name
#   }
# }

resource "kubernetes_deployment" "nexus_deploy" {
  metadata {
    name      = "nexus"
    namespace = kubernetes_namespace.build.metadata[0].name
    labels = {
      app = "nexus"
    }
  }

  spec {
    replicas = 2

    selector {
      match_labels = {
        app = "nexus"
      }
    }

    template {
      metadata {
        labels = {
          app = "nexus"
        }
      }

      spec {
        # service_account_name = kubernetes_service_account.nexus.metadata[0].name
        container {
          name  = "nexus"
          image = "sonatype/nexus3:latest"

          resources {
            limits = {
              cpu    = "2"
              memory = "1Gi"
            }
            requests = {
              cpu    = "2"
              memory = "1Gi"
            }
          }

          port {
            container_port = 8081
          }

          volume_mount {
            mount_path = "/nexus-data"
            name       = "nexus-data"
          }
        }

        volume {
          name = "nexus-data"
          persistent_volume_claim {
            claim_name = "nexus-data"
          }
        }
      }
    }
  }
}