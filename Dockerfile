# Stage 1: Build Maven project
# FROM maven:sapmachine as build
FROM maven:3.8.5-openjdk-8

# RUN apt-get update

WORKDIR /usr/src/app

COPY . /usr/src/app

# Remove the default index filw
# RUN rm /usr/src/app/src/main/resources/application.properties
# Copy your custom index file
# COPY /Docker/src/main/resources/application.properties /usr/src/app/src/main/resources/

# Download the project dependencies
# RUN mvn dependency:go-offline -B

# Build the project
RUN mvn package



# Stage 2: Create the final Docker image
# FROM maven:3.8.5-openjdk-17

# WORKDIR /usr/src/app

# Copy the JAR file from the build stage
# COPY --from=build /usr/src/app/target/Toy0Store-1.0.jar .

# Expose the desired port (replace with your application's port)
ENV PORT 8080

EXPOSE $PORT

# WORKDIR /usr/src/app/target
# CMD java -jar target/Toy0Store-1.0.jar ;

CMD [ "sh", "-c", "java -jar target/Toy0Store-1.0.jar" ]
# CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]