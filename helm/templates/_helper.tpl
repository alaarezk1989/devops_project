
{{- define "app.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "app.labels" -}}
app: {{ .Release.Name }}-{{ .Values.metadata.app.name }} 
helm.sh/chart: {{ include "app.chart" . }}
{{- range $x,$y := .Values.appLabels   }}
{{ $x }}: {{ $y | quote }}
{{- end }}
{{- end }}